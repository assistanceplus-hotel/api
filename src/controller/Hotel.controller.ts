import {Request, Response} from "express";
import {getManager} from "typeorm";
import { Hotel } from "../entity/Hotel.entity";

export const addHotel = async (req: Request, res: Response) => {
    const hotelRepository = getManager().getRepository(Hotel);
    try {
        await hotelRepository.save(req.body);
        res.send(req.body);
    } catch (error) {
        res.send(error.sqlMessage)
    }
};

export const getAllhotels = async (req: Request, res: Response) => {
    const hotelRepository = getManager().getRepository(Hotel);
    const hotel = await hotelRepository.find();
    res.send(hotel);
};

export const getHotelId = async (req: Request, res: Response) => {
    const hotelRepository = getManager().getRepository(Hotel);
    const hotel = await hotelRepository.findOne({
        where: {
            id: req.params.id
        }
    });
    res.send(hotel);
};

export const updateHotel = async (req: Request, res: Response) => {
    const hotelRepository = getManager().getRepository(Hotel);
    const hotel = await hotelRepository.findOne({
        where : {
            id: req.params.id
        }
    })
    if(!hotel) {
        throw new Error("Hotel introuvable")
    }
    
    hotel.denomination = req.body.denomination || hotel.denomination;
    hotel.adresse = req.body.adresse || hotel.adresse;
    hotel.email = req.body.email || hotel.email;
    hotel.logo = req.body.logo || hotel.logo;
    hotel.nif = req.body.nif || hotel.nif;
    hotel.stat = req.body.stat || hotel.stat;
    hotel.rcs = req.body.rcs || hotel.rcs;
    hotel.responsable = req.body.responsable || hotel.responsable;
    hotel.localite = req.body.localite || hotel.localite;
    hotel.statut = req.body.statut || hotel.statut;
    hotel.type = req.body.type || hotel.type;
    hotel.tel = req.body.tel || hotel.tel;
    hotel.tarif = req.body.tarif || hotel.tarif;

    const updateHotel = await hotelRepository.save(hotel);
    res.send(updateHotel);
}

