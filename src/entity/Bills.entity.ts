import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("bills")
export class Bills {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    reference: string;

    @Column()
    date_bill: string;

    @Column()
    begin: string;

    @Column()
    end: string;

    @Column()
    id_room: string;

    @Column()
    id_hotel: string;

    @Column()
    state: string;
    /*
        0 : not paied
        1 : paied
    */

    @Column()
    tarif: string;

    @Column()
    duration: string;

    @Column()
    montant: string;

    @Column()
    ref_facture: string;

    @Column()
    ref_paiement: string;

    @Column()
    mode_reglement: string;

    @Column()
    debut_periode: string;

    @Column()
    fin_periode: string;

    @Column()
    groupId: string;

    @Column()
    createdAt: string;

    @Column()
    updatedAt: string;
}