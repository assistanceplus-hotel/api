import express from "express";
import * as HomeController from "../controller/Home";
import * as HotelController from "../controller/Hotel.controller";
import * as RoomController from "../controller/Room.controller";
import * as CheckingController from "../controller/Checking.controller";
import * as UserController from "../controller/User.controller";
import * as BillController from "../controller/Bills.controller";
import * as FileController from "../controller/UploadFile.controller";

let router = express.Router();

router.get("/", HomeController.showHomePage);

router.post("/hotel",  HotelController.addHotel);
router.get("/hotels",  HotelController.getAllhotels);
router.get("/hotel/:id",  HotelController.getHotelId);
router.put("/hotel/:id",  HotelController.updateHotel);


//Admin
router.get("/users/:id_hotel",  UserController.getUsersHotel);
router.put("/user/:id",  UserController.updateUser);
router.put("/userStatut/:id_hotel",  UserController.updateStatutUserHotel);

//FACTURATION
router.post("/bill",  BillController.addBill);
router.get("/bills",  BillController.getAllBills);
router.get("/bill/:id",  BillController.getBillId);
router.get("/bill_ref/:reference",  BillController.getBillsRef);
router.get("/bill_state/:state/:id_hotel",  BillController.getBillsStatutHotel);
router.get("/bills/:id_hotel",  BillController.getBillsHotel);

router.get("/billsGroup/:id_hotel",  BillController.getBillsGroupRef);
router.get("/billsGroupYear/:id_hotel",  BillController.getBillsGroupRefYear);


router.put("/bill/:id",  BillController.updateBill);
router.put("/bill_ref/:reference",  BillController.updateBillRef);


router.post("/room",  RoomController.addRoom);
router.get("/rooms",  RoomController.getAllrooms);
router.get("/room/:id",  RoomController.getRoomId);
router.get("/rooms/:id_hotel",  RoomController.getRoomsHotel);
router.put("/room/:id",  RoomController.updateRoom);
router.delete("/room/:id",  RoomController.deleteRoom);

router.post("/checking",  CheckingController.addChecking);
router.get("/checkings/:id_room",  CheckingController.getCheckingsRoom);
router.get("/checking/:id",  CheckingController.getCheckingId);
router.get("/checkingOutDuJour/:id_hotel",  CheckingController.getCheckOutDuJour);
router.get("/checkingOutDuOublie/:id_hotel",  CheckingController.getCheckOutOublie);
router.get("/checkingRserve/:id_hotel",  CheckingController.getCheckReserve);
router.get("/checkingsHotel/:id_hotel",  CheckingController.getCheckingsHotel);
router.get("/findPerson/:nom_prenoms",  CheckingController.findPerson);
router.get("/roomDispo/:id_hotel/:id_room/:dateIn/:dateOut",  CheckingController.dispoRoomId);
router.put("/checking/:id",  CheckingController.updateChecking);
router.put("/checkings/:groupId",  CheckingController.updateCheckingGroup);
router.get("/checkProcess/:id_hotel/:stateProcess",  CheckingController.getCheckingStateProcess);

router.get("/checkingBetweenDate/:id_hotel/:dateIn/:dateOut",  CheckingController.getChekingBetweenDates);

//Dashboard
router.get("/getSejours/:id_hotel",  CheckingController.getSejours);
router.get("/getSejours",  CheckingController.getAllSejours);
router.get("/getCI/:id_hotel/:stateProcess",  CheckingController.getCIHotel);
router.get("/getCINomId/:id_hotel/:search_value/:stateProcess",  CheckingController.getCIHotelNom);
router.get("/findPersonInHotel/:search_value",  CheckingController.getPersonInHotel);
router.get("/getAllInHotel/:id_hotel",  CheckingController.getAllInHotel);
router.get("/findPersonInHotel/:id_hotel/:search_value",  CheckingController.findPersonInHotel);
router.get("/findPersonInHotel/:search_value",  CheckingController.findPersonInAllHotel);

//File
router.post("/upload",  FileController.uploadFileController);

export = router