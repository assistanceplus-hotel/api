import { Request, Response } from "express";
import { Between, IsNull, LessThan, Like, MoreThan, Not, getManager } from "typeorm";
import { Checking } from "../entity/Checking.entity";
import { Room } from "../entity/Room.entity";
import { Hotel } from "../entity/Hotel.entity";
import { getRoomId } from "./Room.controller";

export const addChecking = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    try {
        await checkingRepository.save(req.body);
        res.send(req.body);
    } catch (error) {
        res.send(error.sqlMessage)
    }
};

export const getCheckingsHotel = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: {
            id_hotel: req.params.id_hotel
        }
    });
    res.send(checking)
};

export const getCheckOutDuJour = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    let year = new Date().getFullYear();
    let month = ('0' + (new Date().getMonth() + 1)).slice(-2);
    let day = new Date().getDate()
    const checking = await checkingRepository.find({
        where: {
            id_hotel: req.params.id_hotel,
            dateOut: year + '-' + month + '-' + day,
            stateProcess: 'CI'
        }
    });
    res.send(checking)
};

export const getCheckOutOublie = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    let year = new Date().getFullYear();
    let month = ('0' + (new Date().getMonth() + 1)).slice(-2);
    let day = new Date().getDate();
    const checking = await checkingRepository.find({
        where: {
            id_hotel: req.params.id_hotel,
            dateOut: LessThan(new Date(`${year + '-' + month + '-' + day}`)),
            stateProcess: 'CI'
        }
    });
    res.send(checking)
};

export const getCheckReserve = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    let year = new Date().getFullYear();
    let month = ('0' + (new Date().getMonth() + 1)).slice(-2);
    let day = new Date().getDate();
    const checking = await checkingRepository.find({
        where: {
            id_hotel: req.params.id_hotel,
            dateIn: year + '-' + month + '-' + day,
            stateProcess: 'RV'
        }
    });
    res.send(checking)
};

export const findPerson = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: [
            {
                nom_prenoms: Like(`%${req.params.nom_prenoms}%`)
            },
            {
                cin_passport: Like(`%${req.params.nom_prenoms}%`)
            }
        ]
    });
    res.send(checking)
};


export const getCheckingsRoom = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: {
            id_room: req.params.id_room
        }
    });
    res.send(checking)
};


export const dispoRoomId = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        select: ['id'],
        where: {
            id_hotel: req.params.id_hotel,
            id_room: req.params.id_room,
            dateOut: Between(req.params.dateIn, req.params.dateOut),
            stateProcess: Not('CO')
        }
    });
    res.send(checking);
}

function adjustDates(prevBooking, currentBooking) {
    const prevDateOut = new Date(prevBooking.dateOut);
    const currentDateIn = new Date(currentBooking.dateIn);
    if (prevDateOut >= currentDateIn) {
        const newDateIn = new Date(prevDateOut);
        newDateIn.setDate(newDateIn.getDate() + 1);
        currentBooking.dateIn = newDateIn.toISOString().split('T')[0];
    }
}


export const getChekingBetweenDates = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        select: ['id', 'stateProcess', 'dateIn', 'dateOut', 'id_room', 'groupId'],
        where: {
            id_hotel: req.params.id_hotel,
            statut: '0',
            dateOut: Between(req.params.dateIn, req.params.dateOut),
            stateProcess: 'CO'
        }
    });
    // Trier les réservations par date d'entrée
    checking.sort((a, b) => new Date(a.dateIn).valueOf() - new Date(b.dateIn).valueOf());

    let previousBooking = null;
    const occupancyByRoom = new Array();
    for (const booking of checking) {
        if (previousBooking) {
            adjustDates(previousBooking, booking);
        }
        const duration = (new Date(booking.dateOut).valueOf() - new Date(booking.dateIn).valueOf()) / (1000 * 60 * 60 * 24);
        const groupId = booking.groupId;
        if (!occupancyByRoom[booking.id_room]) {
            occupancyByRoom[booking.id_room] = [];
        }

        ///getRoomName
        const roomRepository = getManager().getRepository(Room);
        const room = await roomRepository.findOne({
            where: {
                id: booking.id_room
            }
        });
        //getTarif
        const hotelRepository = getManager().getRepository(Hotel);
        const hotel = await hotelRepository.findOne({
            select: ['tarif'],
            where: {
                id: req.params.id_hotel
            }
        });
        let tarif = hotel.tarif;

        occupancyByRoom.push({
            id: booking.id,
            id_room: booking.id_room,
            room_name: room.code,
            dateIn: booking.dateIn,
            dateOut: booking.dateOut,
            tarif: tarif,
            duration: duration,
            groupId: groupId,
            cout: Number(duration) * Number(tarif)
        });
        previousBooking = booking;
    }
    res.send(occupancyByRoom);
}

export const getCheckingId = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: {
            id: req.params.id
        }
    });
    res.send(checking);
};

export const getCheckingStateProcess = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: {
            stateProcess: req.params.stateProcess,
            id_hotel: req.params.id_hotel
        }
    });
    res.send(checking);
};


export const updateChecking = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.findOne({
        where: {
            id: req.params.id
        }
    })
    if (!checking) {
        throw new Error("Checking introuvable")
    }

    checking.nom_prenoms = req.body.nom_prenoms || checking.nom_prenoms;
    checking.nationality = req.body.nationality || checking.nationality;
    checking.birthday = req.body.birthday || checking.birthday;
    checking.cin_passport = req.body.cin_passport || checking.cin_passport;
    checking.contact = req.body.contact || checking.contact;

    const updateChecking = await checkingRepository.save(checking);
    res.send(updateChecking);
}

export const updateCheckingGroup = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: {
            groupId: req.params.groupId
        }
    })
    if (!checking) {
        throw new Error("Checking introuvable")
    }

    checking.forEach(checking => {
        checking.dateIn = req.body.dateIn || checking.dateIn;
        checking.dateOut = req.body.dateOut || checking.dateOut;
        checking.hourIn = req.body.hourIn || checking.hourIn;
        checking.hourOut = req.body.hourOut || checking.hourOut;
        checking.stateProcess = req.body.stateProcess || checking.stateProcess;
        checking.statut = req.body.statut || checking.statut;
    })

    const updateChecking = await checkingRepository.save(checking);
    res.send(updateChecking);
}

export const getSejours = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    let year = new Date().getFullYear();
    let month = ('0' + (new Date().getMonth() + 1)).slice(-2);
    let day = new Date().getDate()
    const checking = await checkingRepository.find({
        where: {
            id_hotel: req.params.id_hotel,
            dateOut: MoreThan(year + '-' + month + '-' + day),
            dateIn: MoreThan(year + '-' + month + '-' + day)
        }
    });
    res.send(checking)
};

export const getAllSejours = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    let year = new Date().getFullYear();
    let month = ('0' + (new Date().getMonth() + 1)).slice(-2);
    let day = new Date().getDate()
    const checking = await checkingRepository.find({
        where: [
            {
                dateOut: MoreThan(year + '-' + month + '-' + day),
                dateIn: MoreThan(year + '-' + month + '-' + day)
            },
            {
                dateOut: year + '-' + month + '-' + day
            },
            {
                dateIn: year + '-' + month + '-' + day
            }
        ]
    });
    res.send(checking)
};

export const getCIHotel = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: {
            id_hotel: req.params.id_hotel,
            stateProcess: req.params.stateProcess
        }
    });
    res.send(checking)
};

export const getAllInHotel = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: {
            id_hotel: req.params.id_hotel
        }
    });
    res.send(checking)
};

export const findPersonInHotel = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: [
            {
                id_hotel: req.params.id_hotel,
                nom_prenoms: Like(`%${req.params.search_value}%`),
            },
            {
                id_hotel: req.params.id_hotel,
                cin_passport: Like(`%${req.params.search_value}%`),
            }
        ]
    });
    res.send(checking)
};

export const findPersonInAllHotel = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: [
            {
                nom_prenoms: Like(`%${req.params.search_value}%`),
            },
            {
                cin_passport: Like(`%${req.params.search_value}%`),
            }
        ]
    });
    res.send(checking)
};

export const getCIHotelNom = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: [
            {
                id_hotel: req.params.id_hotel,
                nom_prenoms: Like(`%${req.params.search_value}%`),
                stateProcess: 'CI',
            },
            {
                id_hotel: req.params.id_hotel,
                cin_passport: Like(`%${req.params.search_value}%`),
                stateProcess: 'CO'
            }
        ]
    });
    res.send(checking)
};

export const getPersonInHotel = async (req: Request, res: Response) => {
    const checkingRepository = getManager().getRepository(Checking);
    const checking = await checkingRepository.find({
        where: [
            {
                nom_prenoms: Like(`%${req.params.search_value}%`)
            },
            {
                cin_passport: Like(`%${req.params.search_value}%`)
            }
        ]
    });
    res.send(checking)
};