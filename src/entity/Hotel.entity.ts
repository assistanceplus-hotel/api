import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";

@Entity("hotel")
export class Hotel {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({
        unique: true
    })
    denomination: string;

    @Column({
        unique: true
    })
    nif: string;

    @Column({
        unique: true
    })
    stat: string;

    @Column({
        unique: true
    })
    rcs: string;

    @Column()
    email: string;

    @Column()
    tel: string;

    @Column()
    adresse: string;

    @Column()
    logo: string;

    @Column()
    type: string;

    @Column()
    responsable: string;

    @Column()
    localite: string;

    @Column()
    tarif: string;

    @Column()
    statut: string;

    @Column()
    createdAt: string;

    @Column()
    updatedAt: string;
}