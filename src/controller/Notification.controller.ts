import * as SibApiV3Sdk from 'sib-api-v3-sdk';
import esendex from 'esendex';
import { Request, Response } from 'express';

export const sendSms = async (req: Request, res: Response) => {
    const sender = esendex({
        username: req.body.eusername,
        password: req.body.epassword,
    });

    var messages = {
        accountreference: req.body.eaccountref,
        message: [{
          to: req.body.to,
          body: req.body.message,
        }]
      };
      
      sender.messages.send(messages, function (err, response) {
        if (err) {
          return console.log('error: ', err)
        }else {
          return response
        };
      });
}

export const sendMail = async (req: Request, res: Response) => {
    const defaultClient = SibApiV3Sdk.ApiClient.instance;
    let apiKey = defaultClient.authentications['api-key'];
    apiKey.apiKey = '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';

    let apiInstance = new SibApiV3Sdk.TransactionalEmailsApi();
    let sendSmtpEmail = new SibApiV3Sdk.SendSmtpEmail();

    sendSmtpEmail.subject = req.body.subject;
    sendSmtpEmail.htmlContent = req.body.htmlContent;
    sendSmtpEmail.sender = { "name": req.body.name_sender, "email": req.body.email_sender };
    sendSmtpEmail.to = [
        { "email": req.body.email_to, "name": req.body.name_to }
    ];
    sendSmtpEmail.replyTo = { "email": req.body.email_sender, "name": req.body.name_sender };


    apiInstance.sendTransacEmail(sendSmtpEmail).then(function (data) {
        console.log('API called successfully. Returned data: ');
    }, function (error) {
        console.error(error);
    });
}

