import "reflect-metadata";
import express from "express";
import bodyParser, { json } from "body-parser";
import cors from "cors";
import dotenv from 'dotenv'
//controller
import { createConnection } from "typeorm";
import { Checking } from "./entity/Checking.entity";
import { Hotel } from "./entity/Hotel.entity";
import { Room } from "./entity/Room.entity";
import { User } from "./entity/User.entity";
import authRouter from "./controller/Auth.controller";
import { Bills } from "./entity/Bills.entity";
import router from "./router/route";
import authenticateToken from "./middleware/authMiddleware";

const path = require('path');

dotenv.config({ path: __dirname + '/.env' });

createConnection({
  type: "mysql",
  host: process.env.TYPEORM_HOST,
  port: 3306,
  username: process.env.TYPEORM_USERNAME,
  password: process.env.TYPEORM_PASSWORD,
  database: process.env.TYPEORM_DATABASE,
  synchronize: true,
  dropSchema: false,
  entities: [
    Checking,
    Hotel,
    Room,
    User,
    Bills
  ],
  logging: false
}).then(connection => {
  console.log("Hôtel backend application is up and running on port 3010");
}).catch(error => console.log(error));

// create express app
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({
  origin: '*'
}));
app.use('/api', authenticateToken, router);
app.use("/auth", authRouter);

app.use('/uploads', authenticateToken, express.static(path.join(__dirname, 'controller', 'uploads')));

// run app
app.listen(3010);