import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity("room")
export class Room {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    code: string;

    @Column()
    libelle: string;

    @Column()
    description: string;

    @Column()
    client_limit: number;

    @Column()
    id_hotel: string;

    @Column()
    statut: string;

    @Column()
    createdAt: string;

    @Column()
    updatedAt: string;
}