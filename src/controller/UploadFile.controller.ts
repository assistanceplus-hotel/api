const multer = require('multer');
const path = require('path');

// Définir le dossier de destination pour les fichiers téléchargés
const uploadDirectory = path.join(__dirname, 'uploads');

// Configurer multer pour le téléchargement de fichiers
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadDirectory);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

const upload = multer({ storage: storage });

export const uploadFileController = (req, res) => {
    // Utiliser 'upload.single' pour télécharger un seul fichier avec le champ 'file'
    upload.single('file')(req, res, (err) => {
        if (err instanceof multer.MulterError) {
            // Si une erreur de Multer s'est produite
            console.error(err); // Afficher l'erreur dans la console à des fins de débogage
            return res.status(400).send('Une erreur s\'est produite lors du téléchargement du fichier.');
        } else if (err) {
            // Si une autre erreur s'est produite
            console.error(err); // Afficher l'erreur dans la console à des fins de débogage
            return res.status(500).send('Une erreur inattendue s\'est produite.');
        }

        // Le fichier a été téléchargé avec succès
        res.send('Fichier téléchargé avec succès.');
    });
};
