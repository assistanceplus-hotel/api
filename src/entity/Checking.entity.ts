import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany, JoinTable } from "typeorm";

@Entity("checking")
export class Checking {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    nom_prenoms: string;

    @Column()
    nationality: string;

    @Column()
    birthday: string;

    @Column()
    cin_passport: string;

    @Column()
    contact: string;

    @Column()
    dateIn: string;

    @Column()
    hourIn: string;

    @Column()
    dateOut: string;

    @Column()
    hourOut: string;

    @Column()
    statut: string;
    /*
        0: Non Facturé
        1: Annulé
        2: Facturé
    */

    @Column()
    groupId: string;

    @Column()
    stateProcess: string;
    /*
        CI: Check In
        CO: Check Out
        RV: Reservation
    */

    @Column()
    id_hotel: string;

    @Column()
    id_room: string;

    @Column()
    createdAt: string;

    @Column()
    updatedAt: string;
}