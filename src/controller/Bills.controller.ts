import { Request, Response } from "express";
import { getManager } from "typeorm";
import { Bills } from "../entity/Bills.entity";

export const addBill = async (req: Request, res: Response) => {
    const billRepository = getManager().getRepository(Bills);
    try {
        await billRepository.save(req.body);
        res.send(req.body);
    } catch (error) {
        res.send(error.sqlMessage)
    }
};

export const getAllBills = async (req: Request, res: Response) => {
    const billRepository = getManager().getRepository(Bills);
    const bill = await billRepository.find({
        where: {
            id_hotel: req.params.id_hotel
        }
    });
    res.send(bill);
};

export const getBillsRef = async (req: Request, res: Response) => {
    const billRepository = getManager().getRepository(Bills);
    const bill = await billRepository.find({
        where: {
            reference: req.params.reference
        }
    });
    res.send(bill);
};


export const getBillsStatutHotel = async (req: Request, res: Response) => {
    const billRepository = getManager().getRepository(Bills);
    const bill = await billRepository.find({
        where: {
            state: req.params.state,
            id_hotel: req.params.id_hotel
        }
    });
    res.send(bill);
};

export const getBillsHotel = async (req: Request, res: Response) => {
    const billRepository = getManager().getRepository(Bills);
    const bill = await billRepository.find();
    res.send(bill);
};

export const getBillId = async (req: Request, res: Response) => {
    const billRepository = getManager().getRepository(Bills);
    const bill = await billRepository.findOne({
        where: {
            id: req.params.id
        }
    });
    res.send(bill);
};

export const updateBill = async (req: Request, res: Response) => {
    const billRepository = getManager().getRepository(Bills);
    const bill = await billRepository.find({
        where: {
            id: req.params.id
        }
    })
    if (!bill) {
        throw new Error("Bill introuvable")
    }

    bill.forEach(bill => {
        bill.begin = req.body.begin || bill.begin;
        bill.date_bill = req.body.date_bill || bill.date_bill;
        bill.end = req.body.end || bill.end;
        bill.id_hotel = req.body.id_hotel || bill.id_hotel;
        bill.montant = req.body.montant || bill.montant;
        bill.ref_paiement = req.body.ref_paiement || bill.ref_paiement;
        bill.id_room = req.body.id_room || bill.id_room;
        bill.state = req.body.state || bill.state;
    });

    const updateBill = await billRepository.save(bill);
    res.send(updateBill);
}

export const updateBillRef = async (req: Request, res: Response) => {
    const billRepository = getManager().getRepository(Bills);
    const bill = await billRepository.find({
        where: {
            reference: req.params.reference
        }
    })
    if (!bill) {
        throw new Error("Bill introuvable")
    }

    bill.forEach(element => {
        element.ref_paiement = req.body.ref_paiement || element.ref_paiement;
        element.state = req.body.state || element.state;
    });
    const updatedBills = await billRepository.save(bill);
    return res.send(updatedBills);
}

export const getBillsGroupRef = async (req: Request, res: Response) => {
    const billRepository = getManager().getRepository(Bills);
    const bills = await billRepository
    .createQueryBuilder('data')
    .select('data.reference', 'reference')
    .where({
        id_hotel: req.params.id_hotel
    })
    .addSelect('SUM(data.montant)', 'totalAmount')
    .addSelect('data.state', 'state')
    .addSelect('data.date_bill', 'date_bill')
    .addSelect('data.debut_periode', 'debut_periode')
    .addSelect('data.fin_periode', 'fin_periode')
    .groupBy('data.reference')
    .getRawMany();

    res.send(bills);
};

export const getBillsGroupRefYear = async (req: Request, res: Response) => {
    const billRepository = getManager().getRepository(Bills);
    const year = new Date().getFullYear();
    const bills = await billRepository
    .createQueryBuilder('data')
    .select('data.reference', 'reference')
    .addSelect('YEAR(data.date_bill)', 'year')
    .where({
        id_hotel: req.params.id_hotel
    })
    .andWhere(`YEAR(data.date_bill) = :year`, { year })
    .groupBy('data.reference, YEAR(data.date_bill)')
    .getRawMany();

    res.send(bills);
};