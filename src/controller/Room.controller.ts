import {Request, Response} from "express";
import {getManager} from "typeorm";
import { Room } from "../entity/Room.entity";

export const addRoom = async (req: Request, res: Response) => {
    const roomRepository = getManager().getRepository(Room);
    try {
        await roomRepository.save(req.body);
        res.send(req.body);
    } catch (error) {
        res.send(error.sqlMessage)
    }
};

export const getAllrooms = async (req: Request, res: Response) => {
    const roomRepository = getManager().getRepository(Room);
    const room = await roomRepository.find();
    res.send(room);
};

export const getRoomId = async (req: Request, res: Response) => {
    const roomRepository = getManager().getRepository(Room);
    const room = await roomRepository.findOne({
        where: {
            id: req.params.id
        }
    });
    res.send(room);
};

export const getRoomsHotel = async (req: Request, res: Response) => {
    const roomRepository = getManager().getRepository(Room);
    const room = await roomRepository.find({
        where: {
            id_hotel: req.params.id_hotel
        }
    });
    res.send(room);
};

export const getRoomStatut = async (req: Request, res: Response) => {
    const roomRepository = getManager().getRepository(Room);
    const room = await roomRepository.findOne({
        where: {
            statut: req.params.statut
        }
    });
    res.send(room);
};

export const updateRoom = async (req: Request, res: Response) => {
    const roomRepository = getManager().getRepository(Room);
    const room = await roomRepository.findOne({
        where : {
            id: req.params.id
        }
    })
    if(!room) {
        throw new Error("Room introuvable")
    }
    
    room.code = req.body.code || room.code;
    room.description = req.body.description || room.description;
    room.statut = req.body.statut || room.statut;
    room.libelle = req.body.libelle || room.libelle;
    room.client_limit = req.body.client_limit || room.client_limit;

    const updateRoom = await roomRepository.save(room);
    res.send(updateRoom);
}

export const deleteRoom = async (req: Request, res: Response) => {
    const roomRepository = getManager().getRepository(Room);
    const room = await roomRepository.findOne({
        where : {
            id: req.params.id
        }
    })
    if(!room) {
        throw new Error("Consultation introuvable")
    }

    const deleteRoom = await roomRepository.remove(room);
    res.send(deleteRoom.id);
}
