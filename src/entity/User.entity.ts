import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity("user")
export class User {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({
        unique: true
    })
    username: string;

    @Column()
    password: string;

    @Column()
    description: string;

    @Column()
    id_hotel: string;

    @Column()
    role: string;
    /*
        0 : AssistancePlus
        1 : Administrator
        2 : Manager
        3 : Receipt 
        4 : Operateur A+
    */

    @Column()
    statut: string;

    @Column()
    createdAt: string;

    @Column()
    updatedAt: string;
}