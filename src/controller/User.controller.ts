import {Request, Response} from "express";
import {getManager} from "typeorm";
import { User } from "../entity/User.entity";
import * as bcrypt from 'bcryptjs';

export const getAllUsers = async (req: Request, res: Response) => {
    const userRepository = getManager().getRepository(User);
    const users = await userRepository.find();
    res.send(users);
};

export const getUsersHotel = async (req: Request, res: Response) => {
    const userRepository = getManager().getRepository(User);
    const users = await userRepository.find({
        where: {
            id_hotel: req.params.id_hotel
        }
    });
    res.send(users);
};

export const getUserId = async (req: Request, res: Response) => {
    const userRepository = getManager().getRepository(User);
    const users = await userRepository.findOne({
        where: {
            id: req.params.id
        }
    });
    res.send(users);
};

export const updateUser = async (req: Request, res: Response) => {
    const userRepository = getManager().getRepository(User);
    const user = await userRepository.findOne({
        where : {
            id: req.params.id
        }
    })
    if(!user) {
        throw new Error("User introuvable")
    }
    
    user.username = req.body.username || user.username;
   // user.password = passwordMatch || user.password;
    user.role = req.body.role || user.role;
    user.description = req.body.description || user.description;
    user.statut = req.body.statut || user.statut;

    const updateUser = await userRepository.save(user);
    res.send(updateUser);
}


export const updateStatutUserHotel = async (req: Request, res: Response) => {
    const userRepository = getManager().getRepository(User);
    const users = await userRepository.find({
        where : {
            id_hotel: req.params.id_hotel
        }
    })
    if(!users) {
        throw new Error("User introuvable")
    }
    
    users.forEach(user => {
        user.statut = req.body.statut || user.statut;
    })
    
    const updateUser = await userRepository.save(users);
    res.send(updateUser);
}