import { Router, Request, Response } from 'express';
import { getRepository } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { User } from '../entity/User.entity';
import * as HotelController from "../controller/Hotel.controller";

const authRouter = Router();

authRouter.post('/login', async (req: Request, res: Response) => {
    const { username, password } = req.body;
    const userRepository = getRepository(User);

    const user = await userRepository.findOne({ username });

    if (!user) {
        return res.status(401).json({ message: 'Invalid username or password' });
    }

    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
        return res.status(401).json({ message: 'Invalid username or password' });
    }

    const token = jwt.sign({ userId: user.id, description: user.description, id_hotel: user.id_hotel, userRole: user.role, statut: user.statut }, '3QwPqL20WzOEq9wG3qchq46kMEGDE9ob', {
        expiresIn: '2h',
    });
    res.json({ token });
});

authRouter.post('/signup', async (req: Request, res: Response) => {
    const { username, statut, password, role, id_hotel, description } = req.body;
    const userRepository = getRepository(User);

    const existingUser = await userRepository.findOne({ username });

    if (existingUser) {
        return res.status(400).json({ message: 'Username already exists' });
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    const newUser = userRepository.create({
        username,
        statut,
        role,
        description,
        id_hotel,
        password: hashedPassword,
    });

    await userRepository.save(newUser);

    res.status(201).json({ message: 'User registered successfully' });
});

export default authRouter;
